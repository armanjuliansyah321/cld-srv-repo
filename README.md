Tugas Akhir Cloud Computing
DevOps Docker dengan Gitlab
Aplikasi Web Database

Nama:Arman Juliansyah
Aktivitas:SIB BISA AI AI INFRA

log:17-11-2022

DESKRIPSI

Tugas ini menampilkan tampilan website berupa bagian dari microservices cloud computing dengan bahasa program PHP, database MySQL dan Automasi Program dengan docker serta gitlab. Selain itu juga terdapat web config dari dbms yaitu phpmyadmin sebagai mengelola database dari program yang digunakan.

Pada Tugas ini terdapat beberapa bagian yang perlu ditambahkan pada pembuatan container atau docker-compose yaitu env

ENV yang digunakan pada web ini diantaranya:

1.web_host_database
2.web_user_database
3.web_user_password_database
4.web_database

PENGGUNAAN

untuk penggunaan ini disarankan menggunakan konsep dari docker-compose agar memudahkan dalam pembuatannya.
Untuk file docker-compose bisa dilihat pada repository ini.
