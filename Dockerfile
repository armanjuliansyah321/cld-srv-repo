ARG PHP_VERSION=7.4-apache

FROM php:$PHP_VERSION

ENV web_host_database=localhost
ENV web_user_database=root
ENV web_user_password_database=root
ENV web_database=database

RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli

RUN a2enmod rewrite

RUN apt-get update && apt-get upgrade -y

RUN apt-get install nano -y
RUN apt-get install vim -y

WORKDIR /var/www/html

COPY ./prod /var/www/html

EXPOSE 80/tcp
