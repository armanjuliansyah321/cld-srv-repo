<?php
$host = getenv("web_host_database");
$username = getenv("web_user_database");
$password = getenv("web_user_password_database");
$database = getenv("web_database");

$koneksi = mysqli_connect($host, $username, $password, $database);

if (mysqli_connect_errno()) {
    echo "Koneksi database gagal : " . mysqli_connect_error();
}
