if [ "$1" == "build" ];then
docker build -t ${IMAGE_NAME_GLOBAL} -t ${IMAGE_NAME_LOCAL} -f Dockerfile .
docker image ls
docker logout

docker login -u ${username_docker_user} -p ${password_docker_user}
docker push ${IMAGE_NAME_GLOBAL}
docker logout

docker login registry.gitlab.com -u ${username_gitlab_local_user} -p ${token_password_gitlab_local_user}
docker push ${IMAGE_NAME_LOCAL}
docker logout registry.gitlab.com


elif [ "$1" == "deploy" ];then

apt-get update -y && apt-get upgrade -y && apt-get install ssh -y && apt-get install curl -y
cat ${PRIVATE_KEY} >> id_rsa 
chmod 400 id_rsa 

ssh -o "StrictHostKeyChecking=no" -i id_rsa "root@34.83.146.60" "mkdir web-cloud-srv && ls"
scp -i id_rsa ${file_web_deploy}  root@34.83.146.60:${path_web_app}

ssh -o "StrictHostKeyChecking=no" -i id_rsa "root@34.83.146.60" "cd ${path_web_app} && docker-compose up -d && docker-compose ps"

ssh -o "StrictHostKeyChecking=no" -i id_rsa "root@34.83.146.60" "curl -Is ${url_global_test_web_app} | head -1"

fi
